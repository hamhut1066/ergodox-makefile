# This is a simple makefile to help with extracting and installing new ergodox configurations
default: flash backup


flash: unzip
	sudo ls &> /dev/null # before putting in flash mode
	@echo "Flashing Device in 2 seconds, please make sure that you are in flash mode"
	sleep 5
	sudo dfu-util -D output/right_kiibohd.dfu.bin output/left_kiibohd.dfu.bin
	@echo "Flash complete"


unzip: cout
	@echo "Unzipping downloaded zip"
	ls zips | tail -n1 | xargs -I %  unzip "zips/%" -d output


backup: unzip
	@echo "Backing up json config"
	cp output/*.json "./backups/$(shell date +%Y%m%d%H%M%S).json"

clean: cz cout

cout:
	rm -rf ./output/*

cz:
	rm -rf ./zips/*.zip
