##Ergodox Makefile for https://input.club/configurator-ergodox

This is a simple makefile that is designed to be as simple as possible.
It is designed around 3 basic principles, unzipping of new configurations,
flashing of unzipped configurations, and backing up of the 'current' configuration.

This is done through `make unzip`, `make flash` and `make backup` respectively.

I have added an arbitrary `sleep 5` to the flashing procedure,
as that gives me time to put my keyboard into flash mode before it tries to run _dfu-util_.

There is also a `make clean` operation, which will give you easy cleaning of the project. Be careful though,
as when running `make clean` you will lose the contents of both the **zips** and **outputs** directory.
If you want to clean a specific directory, then they are `cz` and `cout` respectively.

###backing up

Backing up is a simple process of taking the MDErgo1-default.json to $(current-date/time).json in _./backups_.
This gives you every version of your configuration easily recoverable (as I am guilty of accidentally deleting a previous config accidentally).

I hope that this simple makefile proves useful in keeping your files up to date.
